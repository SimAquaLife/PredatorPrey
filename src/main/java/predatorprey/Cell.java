package predatorprey;

import fr.cemagref.simaqualife.extensions.spatial2D.Grid2D;
import fr.cemagref.simaqualife.extensions.spatial2D.IndexedCell;
import fr.cemagref.simaqualife.pilot.Pilot;

import java.util.ArrayList;
import java.util.List;

import predatorprey.predators.Predator;
import predatorprey.preys.Prey;

public class Cell extends IndexedCell {

    /**
     * <code>preyCarryingCapacity</code> number of preys that this place can
     * support
     */
    private int preyCarryingCapacity;

    /**
     * <code>habitatQuality</code> The quality of this place (predator point of
     * view). A high value means that preys are vulnerable for the predators
     */
    private double habitatQuality;

    /**
     * <code>preys</code> The list of preys in this place
     */
    private transient List<Prey> preys;

    /**
     * <code>predators</code> The list of predators in this place
     */
    private transient List<Predator> predators;

    public Cell(int index, double habitatQuality, int preyCarryingCapacity) {
        super(index);
        this.habitatQuality = habitatQuality;
        this.preyCarryingCapacity = preyCarryingCapacity;

        preys = new ArrayList<Prey>();
        predators = new ArrayList<Predator>();
    }

    public List<Prey> getPreys() {
        return preys;
    }

    public List<Predator> getPredators() {
        return predators;
    }

    public boolean addPrey(Prey prey) {
        return preys.add(prey);
    }

    public boolean removePrey(Prey prey) {
        return preys.remove(prey);
    }

    public boolean addPredator(Predator predator) {
        return predators.add(predator);
    }

    public boolean removePredator(Predator predator) {
        return predators.remove(predator);
    }

    public double getHabitatQuality() {
        return habitatQuality;
    }

    public int getPreyCarryingCapacity() {
        return preyCarryingCapacity;
    }

}
