package predatorprey.predators;

import fr.cemagref.simaqualife.pilot.Pilot;
import predatorprey.Cell;
import predatorprey.Individual;

public class Predator extends Individual {

	private double ingestedFood=0.;
	
    public Predator(Pilot pilot, Cell cell) {
    	//TODO fix weight according to the weightAtAgeThreshold
        // Default value of weight at birth
    	this(pilot, cell, 0, 10.);
    }
    
    public Predator(Pilot pilot, Cell cell, int age, double weight) {
        super(pilot, cell);
        this.age =age;
        this.weight = weight;
    }
    

	public double getIngestedFood() {
		return ingestedFood;
	}

	public void setIngestedFood(double ingestedFood) {
		this.ingestedFood = ingestedFood;
	}
	public void addIngestedFood(double ingestedFood) {
		this.ingestedFood += ingestedFood;
	}
	
	public double getSuitabilityForPredator(Cell cell){
		if (cell.getPredators().size()>1)
			return 0.; // at least an other predator in the cell
		else
			return((double) cell.getPreys().size()) * cell.getHabitatQuality(); // number of preys accessible
	}
}

