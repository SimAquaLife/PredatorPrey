package predatorprey.predators;

import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;

public class PredatorGrowthProcess extends LoopAquaNismsGroupProcess<Predator,AquaNismsGroup<Predator,?>> {

	/**
	 * <code>convertionFactor</code> proportion of the ingested food transformed into predator weight 
	 */
	private double convertionFactor = 0.25;
	private double slimRate = 0.90;
	
	@Override
	protected void doProcess(Predator predator, AquaNismsGroup<Predator, ?> group) {
		predator.incAge();
		//System.out.print("  "+ (double) predator.getAge()/12. +"y "+predator.getWeight()+ " " );
		predator.setWeight( predator.getWeight() * slimRate + predator.getIngestedFood()* convertionFactor);
		//System.out.print(predator.getIngestedFood() +" " + predator.getWeight());
		//double ratio = 12* predator.getWeight()/(predator.getAge()+12);
		//System.out.println("("+ ((double) Math.round(ratio*100))/100 +")");
		predator.setIngestedFood(0.0);
	}

}
