package predatorprey.predators;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import predatorprey.Cell;
import predatorprey.preys.Prey;
import predatorprey.preys.PreysGroup;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author patrick.lambert
 * 
 */
public class PredatorHuntProcess extends
LoopAquaNismsGroupProcess<Predator, AquaNismsGroup<Predator, ?>> {

	/**
	 * <code>satiety</code> ratio of the predator weigth which stops eating
	 * preys
	 */
	private double satiety = 0.10;

	transient private UniformGen uniformGen;

	@Override
	protected void doProcess(Predator predator,
			AquaNismsGroup<Predator, ?> group) {
		Cell predatorCell = predator.getPosition();
		if (predatorCell.getPreys().size() > 0) {
			//System.out.print(" " +  predatorCell.getPreys().size() +" preys available in cell ");
			//System.out.print("("+predatorCell.getX()+","+predatorCell.getY()+")");
			
			// sort the preys list according to the weigth
			List<Prey> sortedPreys = predatorCell.getPreys();
			Collections.sort(sortedPreys);

			double ratio = predator.getIngestedFood()/predator.getWeight();
			int idx = 0;
			int idxMax =  sortedPreys.size();
			int target=0;
			int eaten=0;
			while ((predator.getIngestedFood()/predator.getWeight() < satiety) && (idx < idxMax)) {

				// the probability for a prey to be eaten depends 
				// on the habitat quality in the cell
				if (uniformGen.nextDouble() < predatorCell.getHabitatQuality()){
					// eat the prey
					Prey eatenPrey = sortedPreys.get(target);
					predator.addIngestedFood(eatenPrey.getWeight());

					// and the prey dies
					
					((PreysGroup)group.getPilot().getAquaticWorld().getAquaNismsGroupsList().get(0)).removeAquaNism(eatenPrey);
					//this.interAquanism(eatenPrey);
					predatorCell.removePrey(eatenPrey);
					eaten ++;
				}
				else
					target ++;

				idx++;
				ratio = predator.getIngestedFood()/predator.getWeight();
			}
			ratio= ((double) Math.round(100*ratio))/100;
			//System.out.println(", " + eaten + " preys eaten ("+ ratio +" ), still " +  predatorCell.getPreys().size());
		}

	}

	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		sorted = true;
		comparator = new Comparator<Predator>() {
			public int compare(Predator o1, Predator o2) {
				return (int) Math.signum(o2.getWeight() - o1.getWeight());
			}
		};

		uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist());
	}

}
