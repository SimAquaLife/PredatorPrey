package predatorprey.predators;

import java.util.ArrayList;
import java.util.List;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;


public class PredatorMortalityProcess extends AquaNismsGroupProcess<Predator, PredatorsGroup>{

	public void doProcess(PredatorsGroup group) {
		List<Predator> deadPredator = new ArrayList<Predator>();
		for (Predator predator: group.getAquaNismsList()){
			double ratio = (12. * predator.getWeight() / (double) (predator.getAge()+12));

			if (ratio < group.getWeightAtAgeThreshold())
				deadPredator.add(predator);
		}
	    // update the predatorsGroup
		for (Predator predator: deadPredator){
			group.getAquaNismsList().remove(predator);
			//ASK WHY
			predator.getPosition().removePredator(predator);
		}
	}
}
