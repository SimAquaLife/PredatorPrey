package predatorprey.predators;

import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;

import java.util.ArrayList;
import java.util.List;

import predatorprey.Cell;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.pilot.Pilot;

public class PredatorMovement extends LoopAquaNismsGroupProcess<Predator, PredatorsGroup> {

    transient private UniformGen uniformGen;

    public PredatorMovement(Pilot pilot) {
        uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist());
    }
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist(0,1));
	}
    
    
    @Override
    protected void doProcess(Predator predator, PredatorsGroup group) {

        final List<Cell> surrounding = group.getEnvironment().
        getNeighbours(predator.getPosition());

        // the first possiblity is the cell where the prey is
        List<Cell> possibilities = new ArrayList<Cell>();
        possibilities.add(predator.getPosition());
        double cellSuitability = predator.getSuitabilityForPredator(predator.getPosition());

        // identify the destination possibilities in the neighbouring
        // with the highest suitability
        for (Cell cell : surrounding) {
            double currentCellSuitability = predator.getSuitabilityForPredator(cell);
            if (currentCellSuitability > cellSuitability) {
                cellSuitability = currentCellSuitability;
                possibilities.clear();
                possibilities.add(cell);
            } else if (currentCellSuitability == cellSuitability) {
                possibilities.add(cell);
            }
        }

        // choose the destination cell
        int possibilitiesNumber = possibilities.size();
        if (possibilitiesNumber == 1) {
            predator.moveTo(group.getPilot(), possibilities.get(0), group);
        } else {
            int idx = (int) Math.floor(uniformGen.nextDouble() * (double) possibilitiesNumber);
            predator.moveTo(group.getPilot(), possibilities.get(idx), group);
        }
    }
}
