package predatorprey.predators;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class PredatorObservationProcess extends AquaNismsGroupProcess<Predator,PredatorsGroup> {

	public void doProcess(PredatorsGroup predatorsGroup) {
		predatorsGroup.calculatePredatorsBiomass();
		predatorsGroup.calculatePredatorsNumber();
		
	}

}
