/**
 * 
 */
package predatorprey.predators;

import java.util.ArrayList;
import java.util.List;

import umontreal.iro.lecuyer.probdist.PoissonDist;
import umontreal.iro.lecuyer.randvar.PoissonGen;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author patrick.lambert
 *
 */
public class PredatorReproductionProcess extends AquaNismsGroupProcess<Predator,PredatorsGroup> {
	
	/**
	 * <code>ageAtFirstReproduction</code> age at first reproduction (in year)
	 */
	private int ageAtFirstReproduction = 5;
	
	/**
	 * <code>predatorFertility</code> mean number of offsprings per predator
	 */
	private double predatorFertility = 1.1;
	
	transient private PoissonGen poissonGen;

	

	public void initTransientParameters(Pilot pilot) {
		poissonGen = new PoissonGen(pilot.getRandomStream(), new PoissonDist(predatorFertility));
		
	}
	
	public void doProcess(PredatorsGroup group) {
		int offspring;
		if (1+ ((group.getPilot().getCurrentTime()-1) % 12) == group.getMonthOfBirth()){
			List<Predator> offsprings = new ArrayList<Predator>();
			List<Predator> deadSpwaners = new ArrayList<Predator>();
			for (Predator predator: group.getAquaNismsList()){
				if (Math.floor((double) predator.getAge()/12.) >= ageAtFirstReproduction){
					// number of offspring
					offspring = poissonGen.nextInt();

					System.out.println("  offspring #: "+ offspring);
					for (int i=0; i<offspring ;i++)
						offsprings.add(new Predator(group.getPilot(), predator.getPosition()));
					// die after reproduction
					//if (offspring > 0) // only if reproduce
						deadSpwaners.add(predator);		
				}	
			}
	    // update the predatorsGroup
		for (Predator predator: deadSpwaners){
			group.getAquaNismsList().remove(predator);
			//ASK WHY
			predator.getPosition().removePredator(predator);
		}

		for (Predator predator: offsprings)
			group.addAquaNism(predator);
			
		}
	}
	
	
}
