package predatorprey.predators;

import predatorprey.Grid;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.Processes;
import fr.cemagref.simaqualife.pilot.Pilot;


public class PredatorsGroup extends AquaNismsGroup<Predator,Grid> {
	
	/**
	 * <code>weightAtAgeThreshold</code> threshold of the ratio of  weight (in g) by age (in year)
	 */
	private double weightAtAgeThreshold = 5;
	
	
	/**
	 * <code>monthOfBirth</code> month of reproduction
	 */
	private int monthOfBirth = 6;
	
	
	@Observable(description = "Predators biomass (g)")
	private transient double predatorsBiomass;
	
	@Observable(description = "Predators number")
	private transient int predatorsNumber;
	
	
	
	public PredatorsGroup(Pilot pilot, Grid arg0, Processes arg1) {
		super(pilot, arg0, arg1);
	}
	
	public int calculatePredatorsNumber(){
		predatorsNumber = this.getAquaNismsList().size();
		
		/*int predatorsNumber2=0;
		for (Cell cell: this.getEnvironment().getCells()){
			predatorsNumber2 += cell.getPredators().size();
		}
		System.out.println(" pred: "+predatorsNumber + " " + predatorsNumber2);*/
		return predatorsNumber;

	}

	public double calculatePredatorsBiomass(){
		predatorsBiomass =0.;
			for(Predator predator : this.getAquaNismsList())
				predatorsBiomass += predator.getWeight();
		return predatorsBiomass;

	}


	public int getMonthOfBirth() {
		return monthOfBirth;
	}

	public double getWeightAtAgeThreshold() {
		return weightAtAgeThreshold;
	}
}
