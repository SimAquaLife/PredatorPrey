package predatorprey.predators;

import predatorprey.Cell;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

public class PredatorsPopulateProcess extends AquaNismsGroupProcess<Predator,PredatorsGroup> {

	private int initialNumberOfPredators =5 ;
	
	transient private UniformGen uniformGen;
	
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		double nbCell = (double) this.getGroup().getEnvironment().getCells().length;
		uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist(0,nbCell-1));
	}

	public void doProcess(PredatorsGroup group) {
		Cell[] cells = group.getEnvironment().getCells();
		for (int i=0; i < initialNumberOfPredators ;i++){
			int ageAtCreation = (int)(12+group.getPilot().getCurrentTime()-group.getMonthOfBirth())%12 +12*(i%5);
			//int ageAtCreation =(int) (12+Pilot.getCurrentTime()-group.getMonthOfBirth())%12;
			double weightAtCreation = 2*group.getWeightAtAgeThreshold() * (ageAtCreation+12) / 12;
			Predator newPredator = new Predator(group.getPilot(), cells[(int)Math.round(uniformGen.nextDouble())], 
					ageAtCreation, weightAtCreation);
    		group.addAquaNism(newPredator);
		}
	}
}
