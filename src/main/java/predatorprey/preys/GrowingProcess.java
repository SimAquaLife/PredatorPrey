package predatorprey.preys;

import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;

public class GrowingProcess extends LoopAquaNismsGroupProcess<Prey,AquaNismsGroup<Prey,?>> {

     /**
     * <code>weightMax</code>  maximal weight of a prey (in g)
     */
    private double weightMax = 5; 
    
    @Override
    protected void doProcess(Prey prey, AquaNismsGroup<Prey, ?> group) {
        prey.incAge();
        double newWeight = prey.getWeight() + prey.getMonthlyGrowthRate();
        if (newWeight <= weightMax)
            prey.setWeight(newWeight);
    }

}
