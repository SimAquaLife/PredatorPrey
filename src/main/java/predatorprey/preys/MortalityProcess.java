package predatorprey.preys;

import predatorprey.Cell;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;


public class MortalityProcess extends AquaNismsGroupProcess<Prey,PreysGroup>{


	
	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 * reduce fish number to the over-density limit
	 */
	public void doProcess(PreysGroup preysGroup) {
		for (Cell cell: preysGroup.getEnvironment().getCells()) {
			for (int i =0; i < (cell.getPreys().size() - cell.getPreyCarryingCapacity());i++) {
				preysGroup.getEnvironment().removeAquaNism(preysGroup.getAquaNismsList().get(0), preysGroup);
				preysGroup.getAquaNismsList().remove(0);
			}
			/*for (Iterator<Prey> iter = cell.getPreys().iterator(); iter.hasNext();) {
				if (cell.getPreys().size() > cell.getPreyCarryingCapacity()){
					preysGroup.getAquaNismsList().remove(iter.next());
					//ASK COMMENT LA LISTE DANS CHAQUE CELLULE EST MIS A JOUR
					iter.remove();
				}
				else
					break;
			}*/
		}
	}
}
