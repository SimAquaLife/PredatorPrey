package predatorprey.preys;

import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import java.util.ArrayList;
import java.util.List;

import predatorprey.Cell;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.pilot.Pilot;

public class Movement extends LoopAquaNismsGroupProcess<Prey, PreysGroup> {

    private double basicMovingRate;
    transient private UniformGen uniformGen;

    @InitTransientParameters
    public void init(Pilot pilot) {
        uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist());
    }

    @Override
    protected void doProcess(Prey prey, PreysGroup group) {
        double movingRate = basicMovingRate;
        double cellSuitability = prey.getSuitabilityForPrey(group, prey.getPosition());
        // the moving rate is increased when there is a predator in the cell or in the vicinity
        if (cellSuitability == 0.) {
            movingRate = 1 - Math.pow(1 - basicMovingRate, 4);
        } else if (cellSuitability == 0.5) {
            movingRate = 1 - Math.pow(1 - basicMovingRate, 2);
        }

        if (uniformGen.nextDouble() <= movingRate) {
            final List<Cell> neighbouring = group.getEnvironment().getNeighbours(prey.getPosition());

            // the first possiblity is the cell where the prey is
            List<Cell> possibilities = new ArrayList<Cell>();
            possibilities.add(prey.getPosition());

            // identify the destination possibilities in the neighbouring
            // with lowest suitability
            for (Cell cell : neighbouring) {
                // verify that there is enough room in the possible destination cell
                if (cell.getPreys().size() < cell.getPreyCarryingCapacity()) {
                    double currentCellSuitability = prey.getSuitabilityForPrey(group, cell);
                    if (currentCellSuitability > cellSuitability) {
                        cellSuitability = currentCellSuitability;
                        possibilities.clear();
                        possibilities.add(cell);
                    } else if (currentCellSuitability == cellSuitability) {
                        possibilities.add(cell);
                    }
                }
            }

            // choose the destination cell
            int possibilitiesNumber = possibilities.size();
            if (possibilitiesNumber == 1) {
                prey.moveTo(group.getPilot(), possibilities.get(0), group);
            } else {
                int idx = (int) Math.floor(uniformGen.nextDouble() * (double) possibilitiesNumber);
                prey.moveTo(group.getPilot(), possibilities.get(idx), group);
            }
        }
    }
}
