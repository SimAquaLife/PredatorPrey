package predatorprey.preys;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class PlopProcess extends AquaNismsGroupProcess<Prey,PreysGroup> {

	private int temporisation = 3000; // in ms
	
	public void doProcess(PreysGroup object) {
    	try {
			Thread.sleep(temporisation);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
