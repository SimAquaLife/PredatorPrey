package predatorprey.preys;

import fr.cemagref.simaqualife.pilot.Pilot;
import java.util.List;

import predatorprey.Cell;
import predatorprey.Individual;

public class Prey extends Individual {

    private final double monthlyGrowthRate;

    public Prey(Pilot pilot, Cell arg0, double g) {
        this(pilot, arg0, g, 0);
    }

    public Prey(Pilot pilot, Cell arg0, double g, int age) {
        super(pilot, arg0);
        // Default value of weight at birth
        this.weight = 1;
        this.monthlyGrowthRate = g;
        this.age = age;
    }

    public double getMonthlyGrowthRate() {
        return monthlyGrowthRate;
    }

    public double getSuitabilityForPrey(PreysGroup group, Cell cell) {
        double suitability = 1.; //no predator in the cell and in it vicinity
        if (cell.getPredators().size() > 0) {
            suitability = 0; // at least one predator in the cell
        } else {
            List<Cell> surrounding = group.getEnvironment().getNeighbours(cell);
            for (Cell neighbourg : surrounding) {
                if (neighbourg.getPredators().size() > 0) {
                    suitability = .5; // at least one predator in the cell vicinity
                    break;
                }
            }
        }
        return suitability;
    }

}
