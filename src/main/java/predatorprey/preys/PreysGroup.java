package predatorprey.preys;

import predatorprey.Grid;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.Processes;
import fr.cemagref.simaqualife.pilot.Pilot;

public class PreysGroup extends AquaNismsGroup<Prey,Grid> {
	
	/**
	 * <code>monthOfBirth</code> month of reproduction 
	 */
	private int monthOfBirth =3;
	
	/**
	 * <code>meanGrowthRate</code> mean growth rate in the population of preys
	 */
	private double meanGrowthRate = 0.1;
	/**
	 *  <code>stdDevGrowthRate</code> standart deviation of growt rate in the population of preys
	 */
	private double stdDevGrowthRate = 0.01;
	
	@Observable(description = "Preys number")
	private transient int preysNumber;
	
	@Observable(description = "Preys biomass (g)")
	private transient double preysBiomass;
	
    public PreysGroup(Pilot pilot, Grid environment, Processes processes) {
        super(pilot, environment, processes);
    }

	public int calculatePreysNumber(){
		preysNumber = this.getAquaNismsList().size();
		
		/*int preysNumber2=0;
		 for (Cell cell:this.getEnvironment().getCells())
			 preysNumber2 += cell.getPreys().size();
			System.out.println(" prey: "+preysNumber + " " + preysNumber2);*/
		return preysNumber;
	}
	
	public double calculatePreysBiomass(){
		preysBiomass =0.;
		for(Prey prey : this.getAquaNismsList())
			preysBiomass += prey.getWeight();
		return preysBiomass;
	}

	public double getMeanGrowthRate() {
		return meanGrowthRate;
	}

	public double getStdDevGrowthRate() {
		return stdDevGrowthRate;
	}

	public int getMonthOfBirth() {
		return monthOfBirth;
	}

}
