package predatorprey.preys;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class PreysObservationProcess extends AquaNismsGroupProcess<Prey,PreysGroup> {

	public void doProcess(PreysGroup preysGroup) {
		preysGroup.calculatePreysBiomass();
		preysGroup.calculatePreysNumber();
		
	}
	

}
