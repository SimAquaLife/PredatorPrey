package predatorprey.preys;

import predatorprey.Cell;
import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.NormalGen;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

public class PreysPopulateProcess extends AquaNismsGroupProcess<Prey, PreysGroup> {

    private int initialNumberOfPreys = 500;

    transient private UniformGen uniformGen;
    transient private NormalGen normalGen;

    @InitTransientParameters
    public void initTransientParameters(Pilot pilot) {

        double nbCell = (double) this.getGroup().getEnvironment().getCells().length;
        uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist(0, nbCell - 1));
        normalGen = new NormalGen(pilot.getRandomStream(), new NormalDist(this.getGroup().getMeanGrowthRate(), this.getGroup().getStdDevGrowthRate()));
    }

    public void doProcess(PreysGroup group) {
        Cell[] cells = group.getEnvironment().getCells();

        // to avoid to put too many preys in the environment 
        int totalCarryingCapacity = 0;
        for (Cell cell : cells) {
            totalCarryingCapacity += cell.getPreyCarryingCapacity();
        }
        initialNumberOfPreys = Math.min(totalCarryingCapacity, initialNumberOfPreys);

        for (int i = 0; i < initialNumberOfPreys; i++) {
            Cell cellOfBirth = cells[(int) Math.round(uniformGen.nextDouble())];
            // to avoid a sur-density in a cell
            while (cellOfBirth.getPreys().size() >= cellOfBirth.getPreyCarryingCapacity()) {
                cellOfBirth = cells[(int) Math.round(uniformGen.nextDouble())];
            }
            double growthRate = normalGen.nextDouble();
            int ageOfBirth = (int) (12 + group.getPilot().getCurrentTime() - group.getMonthOfBirth()) % 12;
            Prey newPrey = new Prey(group.getPilot(), cellOfBirth, growthRate, ageOfBirth);
            group.addAquaNism(newPrey);
        }
    }

}
