package predatorprey.preys;

import java.util.ArrayList;
import java.util.List;

import predatorprey.Cell;
import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.probdist.PoissonDist;
import umontreal.iro.lecuyer.randvar.NormalGen;
import umontreal.iro.lecuyer.randvar.PoissonGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

//ASK modify to LoopAquaNismsGroupProcess<Prey,PreysGroup>
public class ReproductionProcess extends AquaNismsGroupProcess<Prey, PreysGroup> {

    /**
     * <code>densityThreshold</code> minimum number of fish necessary to start
     * mating process
     */
    private int densityThreshold;

    /**
     * <code>preyFertility</code> number of offsprings per prey
     */
    private double preyFertility;

    transient private NormalGen normalGen;

    transient private PoissonGen poissonGen;

    public void doProcess(PreysGroup group) {
		// We are forced to do the loop instead of use a
        // LoopAquaNismsGroupProcess, because we need to modify the list, see
        // bug #21
		/*
         * List<Prey> offsprings = new ArrayList<Prey>(); for (Prey prey :
         * group.getAquaNismsList()) { Cell currentCell = prey.getPosition(); if
         * ((Pilot.getCurrentTime() % 12 == group.getMonthOfBirth()) &&
         * (currentCell.getPreys().size() > densityThreshold)) { // determine
         * how many offstrimgs can still be produced int maxOffspringNumber =
         * Math.max(currentCell.getPreyCarryingCapacity() -
         * currentCell.getPreys().size(), 0); // determine the effective number
         * of offspring offspring = Math.min(poissonGen.nextInt(),
         * maxOffspringNumber); for (int i = 0; i < offspring; i++) offsprings
         * .add(new Prey(currentCell, normalGen.nextDouble(),
         * group.getBasicMovingRate())); } } for (Prey prey: offsprings)
         * group.addAquaNism(prey);
         */

        if (1 + ((group.getPilot().getCurrentTime() - 1) % 12) == group.getMonthOfBirth()) {
			// int totalOffspring =0;
            // int initialEffctif = group.getAquaNismsList().size();
            List<Prey> offsprings = new ArrayList<Prey>();
            for (Cell cell : group.getEnvironment().getCells()) {
                if (cell.getPreys().size() >= densityThreshold) {
					// determine how many offstrimgs can still be produced in
                    // this cell
                    int maxOffspringNumber = Math.max(cell
                            .getPreyCarryingCapacity()
                            - cell.getPreys().size(), 0);
                    int idx = 0;
                    int offspring = 0;
					// determine the effective number of offsprings in this cell
                    // according
                    // to the number of preys
                    while (offspring < maxOffspringNumber
                            && idx < cell.getPreys().size()) {
                        offspring = Math.min(offspring + poissonGen.nextInt(),
                                maxOffspringNumber);
                        idx++;
                    }
                    for (int i = 0; i < offspring; i++) {
                        offsprings.add(new Prey(group.getPilot(), cell, normalGen.nextDouble()));
                    }

                    // totalOffspring+= offspring;
                }
            }
			// System.out.println(" total" + totalOffspring + " (" +
            // (double)totalOffspring/(double)initialEffctif+")");
            for (Prey prey : offsprings) {
                group.addAquaNism(prey);
            }
        }
    }

    @InitTransientParameters
    public void initTransientParameters(Pilot pilot) {
        normalGen = new NormalGen(pilot.getRandomStream(), new NormalDist(this
                .getGroup().getMeanGrowthRate(), this.getGroup()
                .getStdDevGrowthRate()));
        poissonGen = new PoissonGen(pilot.getRandomStream(), new PoissonDist(
                preyFertility));
    }

}
